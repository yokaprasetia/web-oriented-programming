<!DOCTYPE html>
<html>
 <head>
    <title>PHP 12C</title>
 </head>
 <body>
    <h1>Associative Arrays</h1>
    <?php
        $dict1 = array('a' => 1, 'b' => 2);
        $dict2 = $dict1;
        $dict1['b'] = 4;
        echo "\$dict1['b'] = ", $dict1['b'],"<br>\n";
        echo "\$dict2['b'] = ", $dict2['b'],"<br>\n";
    ?>
</body>
</html>
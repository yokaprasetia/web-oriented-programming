<!DOCTYPE html>
<html lang='en-GB'>
    <head>
        <title>PHP 12A</title>
    </head>
    <style>
        div{
            color: red;
            font-weight: bold;
        }
    </style>
    <body>
        <h1>Variables and Constants</h1>
        <?php
            error_reporting( E_ALL );
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);

            echo "<h2>Exercise 1a</h2>\n";
            echo "(1) The value of \$userAge is: $userAge<br>\n";
            echo "(2) The value of \$userAge is: ",var_dump($x),"<br>\n";
            echo "(3) This statement is executed<br>\n";
            $userAge = 27;
            echo "(4) The value of \$userAge is: $userAge<br>\n";

            echo "<h2>Exercise 1c</h2>\n";
            if (FALSE) {
                $x = 519; 
            } else {
                echo "(1) The value of \$x is $x<br>\n";
                echo "(2) The value of \$x is ",var_dump($x),"<br>\n"; 
            }
            echo "<div>";
            
            echo "<h2>Exercise 2</h2>\n";
            define("PI",3.14159);
            define("SPEED_OF_LIGHT",299792458,true);
            print "1 - Value of PI: PI<br>\n";
            print "2 - Value of PI: ".PI."<br>\n";
            $diameter = 2;
            $time = 3;
            $circumference1 = PI * $diameter;
            $circumference2 = PI * $diameter;
            $distance = SPEED_OF_LIGHT * $time;
            echo "Diameter = $diameter => ",
            "Circumference1 = $circumference1 | ",
            "Circumference2 = $circumference2<br>\n";
            echo "Time = $time => Distance = $distance<br>\n";
            echo "</div>";
        ?>
    </body>
</html>
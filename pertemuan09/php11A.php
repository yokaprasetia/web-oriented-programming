<!DOCTYPE html>
<html lang="en-GB">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hello World</title>
</head>
<body>
    <h1>Our First PHP Script</h1>
    <?php
        $user = "Yoka Prasetia";
        print("<p><b>Hello $user<br>\nHello World</b></p>");
        
        echo "<h2>Exercise 2a</h2>\n";
        $text = "stop";
        echo  'Single-quotes: ','don\'t \'don\'t\' "don\'t" $text', "<br>\n";
        echo "Double-quotes: ','don't 'don't' \"don\'t\" $text", "<br>\n";
        echo 'Single-quotes: ','glass\\table glass\table glass\ntable',"<br>\n";
        echo "Double-quotes: ","glass\\table glass\tableglass\ntable", "<br>\n";

        echo "<h2>Exercise 2b</h2>\n";
        $student_id = "200846369";
        $staff_id = "E10481370";
        $student_id++;
        $staff_id++;
        echo "1: \$student_id = $student_id<br>";
        echo "1: \$staff_id = $staff_id <br>";
        $student_id += 1;
        // $staff_id += 1;
        echo "2: \$student_id = $student_id<br>";
        echo "2: \$staff_id = $staff_id <br>";

        echo "<h2>Exercise 2c</h2>\n";
        if (FALSE) {
            error_reporting( E_ALL );
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
        }
        echo "Logarithm of 0: ", log(0), "<br>\n";
        echo "Square root of -2: ", sqrt(-2), "<br>\n";
        // echo "Dividing 1 by 0: ", 1/0, "<br>\n";
        // echo "Dividing 0 by 0: ", 0/0, "<br>\n";

        echo "<h2>Exercise 2f</h2>\n";
        function sign($n) {
            return ($n > 0) - ($n < 0);
        }
        function toString($res) {
            return ($res) ? 'TRUE' : 'FALSE';
        }
        foreach (array(-4,0,4) as $i) {
            echo "<h3>Computations for $i</h3>\n";
            // $d is the square root of $i
            $d = sqrt($i);
            // $e is 4 divided by $d
            if($i == 0){
                continue;
            } else {
                $e = 4 / $d;
            }
            // $f is $e rounded
            $f = round($e);
            echo "sqrt($i) = $d<br>\n";
            echo "4/$d = $e<br>\n";
            echo "round($e) = $f<br>\n";
            echo "$e > 0 : ",toString($e > 0), "<br>\n";
            echo "$e <= 0 : ",toString($e <= 0),"<br>\n";
            for ($g = 0.4; $g < 0.7; $g = $g + 0.1) {
                $r = $i + $g * sign($i);
                // $s is $r rounded
                $s = round($r);
                echo "round($r) = $s<br>\n";
            }
        }

        echo "<h2>Exercise 2g</h2>\n";
        $x = 0.3 - 0.2;
        $y = 0.2 - 0.1;
        echo "\$x = $x<br>";
        echo "\$y = $y<br>";
        echo "(\$x == \$y) = ",toString($x == $y),"<br>\n"; 
        echo "(\$x == 0.1) = ",toString($x == 0.1),"<br>\n"; 
        echo "(\$y == 0.1) = ",toString($y == 0.1),"<br>\n";
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang='en-GB'>
    <head>
        <title>PHP14 D</title>
    </head>
    <body>
        <?php
            // First stage: Entry of 'Item' information
            function formItem() {
                echo '<form action="php14D.php" method="post">';
                echo '<label>Item: <input type="text" name="item"></label> 
                </form>'; // Mengggunakan atribut required agar mengeluarkan notifikasi ketika input tidak diisi
            }
            
            // Second stage: Entry of 'Address' information
            function formAddress() {
                if(!empty($_REQUEST['item'])) { // Mengecek isian inputan item apakaah kosong(empty) atau sudah terisi
                    echo '<form action="php14D.php" method="post">'; // jika sudah terisi, maka masuk ke prosedur ini
                    echo '<input type="hidden" name="item" value="', $_REQUEST['item'],'">'; // type hidden digunakan agar nilai dari inputan 'item' dapat ditampilkan di function processInputs()
                    echo ' <label>Address: <input type="text" name="address"></label>
                    </form>'; 
                } else {
                    formItem(); // jika masih kosong, maka function formItem() dipanggil, dan mengeuarkan notifikasi
                    echo 'Masukkan input item terlebih dahulu';
                }
            }

            // Third stage: Processing of `Item' and `Address' information
            function processInputs() {
                if(!empty($_REQUEST['address'])) { // Mengecek isian inputan address apakaah kosong(empty) atau sudah terisi
                    echo 'Item: ', $_REQUEST['item'], '<br>'; // jika sudah terisi, maka masuk ke prosedur ini
                    echo 'Address: ', $_REQUEST['address'], '<br>';
                } else {
                    formAddress(); // jika masih kosong, maka function formAddress() dipanggil, dan mengeluarkan notifikasi
                    echo 'Masukkan address terlebih dahulu';
                }
            }
            
            if (isset($_REQUEST['address'])) {
                // Executing third stage
                processInputs();
            } elseif (isset($_REQUEST['item'])) {
                // Executing second stage
                formAddress();
            } else {
                // Executing first stage
                formItem();
            }
        ?>
    </body>
</html>
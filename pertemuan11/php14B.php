<!DOCTYPE html>
<html lang='en-GB'>
    <head>
        <title>PHP14 B</title>
    </head>
    <body>
        <?php
            session_start();
            if (isset($_REQUEST['item']))
                $_SESSION['item'] = $_REQUEST['item']; // menggunakan session pada baris ini
            echo 'Item: ', $_REQUEST['item'], "<br>";
            echo '<form action="php14C.php" method="post">'; // meneruskan ke file php14C.php
            
            echo '<input type="hidden" name="item" value="', $_REQUEST['item'],'">'; // jika menggunakan hidden type
            echo '<label>Address: <input type="text" name="address"></label></form>';
        ?>
    </body>
</html>
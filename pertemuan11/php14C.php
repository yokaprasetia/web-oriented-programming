<!DOCTYPE html>
<html lang='en-GB'>
    <head>
        <title>PHP14 C</title>
    </head>
    <body>
        <?php
            session_start();
            if (isset($_REQUEST['address']))
                $_SESSION['address'] = $_REQUEST['address'];
            echo 'Item: ', $_SESSION['item'], ' Ini contoh menggunakan SESSION<br>';
            echo 'Address: ', $_SESSION['address'], ' Ini contoh menggunakan SESSION<br>';    
            echo 'Item: ', $_REQUEST['item'], ' Ini contoh menggunakan Type Hidden<br>';
            echo 'Address: ', $_REQUEST['address'], '<br>';
            session_unset();
            session_destroy();
        ?>
    </body>
</html>
<!DOCTYPE html>
<html lang='en-GB'>
    <head>
        <title>PHP14 E</title>
    </head>
    <body>
        <?php
            session_start();
            // First stage: Select a sport
            function selectSport() {
                $sports = array('Football','Rugby');
                echo '<form action="php14E.php" method="post">
                <label>Sport:</label>
                <select name="sport">
                <option value="">Select a sport</option>';
                foreach($sports as $value) {
                    echo '<option value="', $value,'">', $value,'</option>';               
                }
                echo ' </select>
                <input type="submit">
                </form>';
            }
            // Second stage: Select a team
            function selectTeam() {
                if(!empty($_SESSION['sport'])) {
                    $teams = array('Football' => array('Arsenal','Liverpool'), 'Rugby' => array('St Helens','Warrington'));
                    echo 'Sport: ', $_SESSION['sport'], '<br>';
                    echo '<form action="php14E.php" method="post">';
                //  echo '<input type="hidden" name="sport" value="', $_REQUEST['sport'],'">';
                    echo ' <label>Team:</label>
                    <select name="team">
                    <option value="">Select a team</option>';
                    if($_SESSION['sport'] == "Rugby") { // Mengecek pilihan user apakah sama dengan 'Rugby' atau tidak
                        foreach($teams as $sport => $team) {    // jika iya, masuk ke sini looping isi dari array
                            if($sport == "Rugby"){  // cek apakah isi array $teams menampilkan Rugby atau tidak
                                foreach($team as $value) {
                                    echo '<option value="', $value,'">', $value,'</option>';    // jika iya tampilkan array kedua (Nama Kota) dari Rugby
                                }
                                break; // keluar fungsi (foreach)
                            } else { // jika array $teams tidak menampilkan Rugby
                                continue; // lanjut ke looping tahap selanjutnya
                            }
                        }
                    } else { // jika pilihan user bukan 'Rugby'
                        foreach($teams as $sport => $team) { // looping isi array 
                            if($sport == "Football"){ // cek apakah isi array $teams menampilkan Football atau tidak
                                foreach($team as $value) {
                                    echo '<option value="', $value,'">', $value,'</option>'; // Jika Iya, tampilkan array kedua (Nama Kota) dari Football
                                }
                                break; // keluar fungsi (foreach)
                            } else { // jika array $teams tidak menampilkan Football
                                continue; // lanjut ke looping tahap selanjutnya
                            }
                        }
                    }
                    echo ' </select>
                    <input type="submit"> </form>';
                } else {
                    selectSport();
                    echo 'Pilih salah satu sport terlebih dahulu';
                }
                
            }
            // Third stage: Processing of `Sport' and `Team' information
            function processInputs() {
                if(!empty($_SESSION['team'])) {
                    echo 'Sport: ', $_SESSION['sport'], '<br>';
                    echo 'Team: ', $_SESSION['team'], '<br>';
                } else {
                    selectTeam();
                    echo 'Pilih salah satu team terlebih dahulu';
                }
                
            }
            if (isset($_REQUEST['team'])) {
                // Executing third stage
                $_SESSION['team'] = $_REQUEST['team'];
                processInputs();
            } elseif (isset($_REQUEST['sport'])) {
                // Executing second stage
                $_SESSION['sport'] = $_REQUEST['sport'];
                selectTeam();
            } else {
                // Executing first stage
                selectSport();
            }
        ?>
    </body>
</html>